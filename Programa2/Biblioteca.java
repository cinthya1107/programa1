/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package biblioteca;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

/**
 *
 * @author EQUIPO
 */
public class Biblioteca {

    /**
     * @param args the command line arguments
     */
    // Te faltaría realmetne trabajar con el libro y la revista que son atributos de Biblioteca. Por ejemplo, al crear unlibro (función caso1()), Cuando creas el libro=new.........., 
    //faltaría que hicieras un Biblioteca.libro = libro, así asignas al atributo el objeto que acabas de crear. O directamente un Biblioteca.libro = new (........................ 
    //Además, el código se puede resumir un poco para evitar copia-pega en caso1() y caso2(), cuando hay libro y se quiere sobreescribir, y cuando no hay.
    /**
     * @param args the command line arguments
     */
    static Libro libro; // Declaro las variables estaticas para poderlas utilizar en todas las funciones
    // de la clase Bilioteca (las variables de las clases)
    static Revista revista;
    static Autoria autoria;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int eleccion = 1;
        while (eleccion != 0) {// Para que el menu se imprima infinitamente, hasta que el usuario pulse el 0
            System.out.println("1. Crea el libro\n" + "2. Crea la revista\n" + "3. Presta el libro\n"
                    + "4. Devuelve el libro\n" + "5. Presta la revista\n" + "6. Devuelve la revista\n" + "0. Salir");

            eleccion = sc.nextInt();
            if (eleccion > 6 || eleccion < 0) {// Control de errores: para que no se acepten numeros negativos ni
                // mayores al 6
                System.out.println("Introduzca una opcion valida");
            } else {
                switch (eleccion) {
                    case 1:// Crear el objeto libro
                        caso1();
                        break;

                    case 2:// Crear el objeto revista
                        caso2();
                        break;

                    case 3:// Usar el metodo prestar() en libro
                        caso3();
                        break;

                    case 4:// Usar el metodo devolver() en libro
                        caso4();
                        break;

                    case 5:// Usar el metodo prestar() en revista
                        caso5();
                        break;

                    case 6:// Usar el metodo devolver() en revista
                        caso6();
                        break;
                }
            }

        }
    }

    /**
     * *
     * Lee los datos que va introduciendo el usuario y los va almacenando para
     * crear el objeto libro y autoria
     */
    public static void caso1() {
        Scanner sc = new Scanner(System.in);
        if (libro != null) {// Si el libro no existe se crea, sino se pregunta antes de reemplazarlo
            System.out.println("Introduce el titulo");
            String titulo = sc.nextLine();

            System.out.println("Introduce el numero de ejemplares");
            int ejemplares = sc.nextInt();
            sc.nextLine();
            System.out.println("Introduce el nombre");
            String nombre = sc.nextLine();

            System.out.println("Introduce los apellidos");
            String apellidos = sc.nextLine();

            System.out.println("Introduce el dia");
            String dia = sc.nextLine();

            System.out.println("Introduce el mes");
            String mes = sc.nextLine();

            System.out.println("Introduce el año");
            String anio = sc.nextLine();

            String fecha = dia + "/" + mes + "/" + anio;// reuno los datos y los guardo en una unica variable 
             

            try {
                LocalDate fechaNacimiento = LocalDate.parse(fecha, DateTimeFormatter.ofPattern("d/M/yyyy"));// Configuro el LocalDate
                
                Biblioteca.autoria = new Autoria(nombre, apellidos, fechaNacimiento);// Creo el objeto autoria
                Biblioteca.libro = new Libro(titulo, autoria, ejemplares);// Creo el objeto libro
                System.out.println("Has creado el libro");
            } catch (DateTimeParseException e) {
                System.out.println("fecha erronea");// Si la fecha introducida no es correcta, no esta escrita en el
                // formato pedido

            }
        } else {
            System.out.println("Ya tienes un objeto creado, ¿Deseas reemplazarlo?");
            String remplazar = sc.nextLine();
            switch (remplazar) {
                case "Si":
                    System.out.println("Introduce el titulo");
                    String titulo = sc.nextLine();

                    System.out.println("Introduce el numero de ejemplares");
                    int ejemplares = sc.nextInt();
                    sc.nextLine();
                    System.out.println("Introduce el nombre");
                    String nombre = sc.nextLine();

                    System.out.println("Introduce los apellidos");
                    String apellidos = sc.nextLine();

                    System.out.println("Introduce el dia");
                    String dia = sc.nextLine();

                    System.out.println("Introduce el mes");
                    String mes = sc.nextLine();

                    System.out.println("Introduce el año");
                    String anio = sc.nextLine();

                    String fecha = dia + "/" + mes + "/" + anio;// reuno los datos y los guardo en una unica variable

                    try {
                        LocalDate fechaNacimiento = LocalDate.parse(fecha, DateTimeFormatter.ofPattern("d/M/yyyy"));// Configuro
                        // el
                        // LocalDate
                        Autoria autoria = new Autoria(nombre, apellidos, fechaNacimiento);// Creo el objeto autoria
                        Libro libro = new Libro(titulo, autoria, ejemplares);// Creo el objeto libro
                        System.out.println("Has creado el libro");
                    } catch (DateTimeParseException e) {
                        System.out.println("fecha erronea");// Si la fecha introducida no es correcta, no esta escrita en el
                        // formato pedido

                    }
                case "No":
                    System.out.println("Se mantiene el libro original");
            }

        }
    }

    /**
     * *
     * Lee los datos que va introduciendo el usuario y los va almacenando para
     * crear el objeto revista y autoria
     */
    public static void caso2() {
        Scanner sc = new Scanner(System.in);
        if (libro != null) {// Si el libro no existe se crea, sino se pregunta antes de reemplazarlo
            System.out.println("Introduce el titulo");
            String titulo = sc.nextLine();

            System.out.println("Introduce el numero de prestados");
            int prestados = sc.nextInt();
            sc.nextLine();
            System.out.println("Introduce el nombre");
            String nombre = sc.nextLine();

            System.out.println("Introduce los apellidos");
            String apellidos = sc.nextLine();

            System.out.println("Introduce el dia");
            String dia = sc.nextLine();

            System.out.println("Introduce el mes");
            String mes = sc.nextLine();

            System.out.println("Introduce el año");
            String anio = sc.nextLine();

            String fecha2 = dia + "/" + mes + "/" + anio;

            try {
                LocalDate fechaNacimiento = LocalDate.parse(fecha2, DateTimeFormatter.ofPattern("d/M/yyyy"));
                Biblioteca.autoria = new Autoria(nombre, apellidos, fechaNacimiento);
                Biblioteca.revista = new Revista(titulo, autoria);
                System.out.println("Has creado la revista");
            } catch (DateTimeParseException e) {
                System.out.println("fecha erronea");
            }
        } else {
            System.out.println("Ya tienes un objeto creado, ¿Deseas reemplazarlo?");
            String remplazar = sc.nextLine();
            switch (remplazar) {
                case "Si":
                    System.out.println("Introduce el titulo");
                    String titulo = sc.nextLine();

                    System.out.println("Introduce el numero de ejemplares");
                    int ejemplares = sc.nextInt();
                    sc.nextLine();
                    System.out.println("Introduce el nombre");
                    String nombre = sc.nextLine();

                    System.out.println("Introduce los apellidos");
                    String apellidos = sc.nextLine();

                    System.out.println("Introduce el dia");
                    String dia = sc.nextLine();

                    System.out.println("Introduce el mes");
                    String mes = sc.nextLine();

                    System.out.println("Introduce el año");
                    String anio = sc.nextLine();

                    String fecha = dia + "/" + mes + "/" + anio;// reuno los datos y los guardo en una unica variable

                    try {
                        LocalDate fechaNacimiento = LocalDate.parse(fecha, DateTimeFormatter.ofPattern("d/M/yyyy"));// Configuro
                        // el
                        // LocalDate
                        Autoria autoria = new Autoria(nombre, apellidos, fechaNacimiento);// Creo el objeto autoria
                        Libro libro = new Libro(titulo, autoria, ejemplares);// Creo el objeto libro
                        System.out.println("Has creado el libro");
                    } catch (DateTimeParseException e) {
                        System.out.println("fecha erronea");// Si la fecha introducida no es correcta, no esta escrita en el
                        // formato pedido

                    }
                case "No":
                    System.out.println("Se mantiene el libro original");
            }

        }

    }

    /**
     * *
     * Presta libros si hay libros disponibles
     */
    public static void caso3() {
        if (libro != null) {
            int prestar = libro.prestar();

            if (prestar == -1) {
                System.out.println("No puedes prestar libros");

            } else {
                System.out.println("Has prestado el libro");
            }
        } else {
            System.out.println("No existen libros");

        }

    }

    /**
     * *
     * Devuelve libros si hay libros disponibles
     */
    public static void caso4() {
        if (libro != null) {
            int devolver = libro.devolver();

            if (devolver == -1) {
                System.out.println("No puedes devolverlibros");

            } else {
                System.out.println("Has devuelto el libro");
            }
        } else {
            System.out.println("No existen libros");

        }
    }

    /**
     * *
     * Presta revistas si hay revistas disponibles
     */
    public static void caso5() {
        if (revista != null) {
            int prestar = libro.prestar();

            if (prestar == -1) {
                System.out.println("No puedes prestar revistas");

            } else {
                System.out.println("Has prestado una revistas");
            }
        } else {
            System.out.println("No existen revistas");

        }
    }

    /**
     * *
     * Devuelve revistas si hay revistas disponibles
     */
    public static void caso6() {
        if (revista != null) {
            int devolver = revista.devolver();

            if (devolver == -1) {
                System.out.println("No puedes devolver revista");

            } else {
                System.out.println("Has devuelto la revista");
            }
        } else {
            System.out.println("No existen revistas");

        }
    }

}
