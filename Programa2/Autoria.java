package Programacion2;

import java.time.LocalDate;

/***
 * 
 * @author Cinthya
 *
 */

public class Autoria {
	//Atributos
	private String nombre;
	private String apellidos;
	private LocalDate fechaNacimiento;

	// Getters: ya que las clases son privadas son necesarios para acceder a los datos
	
	/***
	 * 
	 * @return nombre
	 */
	public String getNombre() {
		return nombre;
	}
	

	/***
	 * 
	 * @return apellidos
	 */
	public String getApellidos() {
		return apellidos;
	}
	

	/***
	 * 
	 * @return fechaNacimiento
	 */
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	// Setters: ya que los datos son privados, son necesarios para modificar los datos

	/***
	 * 
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/***
	 * 
	 * @param apellidos
	 */
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	/***
	 * 
	 * @param fecha
	 */
	public void setFechaNacimiento(LocalDate fecha) {
		this.fechaNacimiento = fechaNacimiento;
	}

//Constructor
	/***
	 * 
	 * @param nombre
	 * @param apellidos
	 * @param fechaNacimiento
	 */
	public Autoria(String nombre, String apellidos, LocalDate fechaNacimiento) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.fechaNacimiento = fechaNacimiento;
	}
}


