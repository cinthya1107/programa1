package Programacion2;

import java.time.LocalDate;

/***
 * 
 * @author Cinthya
 *
 */

public class Libro {

	// Atributos
    private String titulo;
    private Autoria autoria;
    private int ejemplares;
    private int prestados;
    
    
//Getters: ya que las clases son privadas son necesarios para acceder a los datos
    
    /**
     * *
     *
     * @return titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * *
     *
     * @return ejemplares
     */
    public int getEjemplares() {
        return ejemplares;
    }

    /***
     * 
     * @return prestados
     */
    public int getPrestados() {
        return prestados;
    }
    
    /***
     * 
     * @return
     */
    public Autoria getAutoria() {
        return autoria;
    }
    
//Setters: ya que los datos son privados, son necesarios para modificar los datos
    
    /***
     * 
     * @param titulo 
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /***
     * 
     * @param ejemplares 
     */
    public void setEjemplares(int ejemplares) {
        this.ejemplares = ejemplares;
    }
    
    /***
     * 
     * @param prestados 
     */
    public void setPrestados(int prestados) {
        this.prestados = prestados;
    }
    
    /***
     * 
     * @param autoria
     */
    public void setAutoria(Autoria autoria) {
        this.autoria = autoria;
    }


//Constructor   
    
    /***
     * 
     * @param titulo
     * @param ejemplares
     * @param prestados
     */
    public Libro(String titulo, int ejemplares, int prestados) {
        this.titulo = titulo;
        this.ejemplares = ejemplares;
        this.prestados = prestados;
        this.autoria = autoria;
    }

    /***
     * 
     * @param titulo
     * @param autoria
     * @param ejemplares
     */
    public Libro(String titulo, Autoria autoria, int ejemplares) {
        this.titulo = titulo;
        this.autoria = autoria;
        this.ejemplares = ejemplares;
    }

//M�todos
    
    /***
     * 
     * @return el n�mero de ejemplares menos el prestado .Si no hay libros que prestar, devuelve -1
     */
    public int prestar() {
        if (prestados == ejemplares) {
            return -1;
        } else if (ejemplares == 0) {//si no hay libros, no se pueden prestar
            return - 1;
        } else {
            return ejemplares - 1;
        }
    }

    /***
     * 
     * @return "ejemplares" si hay libros prestados que devolver, sino devuelve el valor -1
     */
    public int devolver() {
        if (prestados > 0) {
            prestados--;
            ejemplares++;
            return ejemplares;
        } else {
            return -1;
        }
    }
}



