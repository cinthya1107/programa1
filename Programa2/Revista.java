package Programacion2;

import java.time.LocalDate;

/***
 * 
 * @author Cinthya
 *
 */

public class Revista {
	
	// Atributos
    private String titulo;
    private Autoria autoria;
    private int prestados;
    
//Getters: ya que las clases son privadas son necesarios para acceder a los datos
    /***
     * 
     * @return tiulo
     */
    public String getTitulo() {
        return titulo;
    }

    /***
     * 
     * @return prestados
     */
    public int getPrestados() {
        return prestados;
    }
    /***
     * 
     * @return
     */
    public Autoria getAutoria() {
        return autoria;
    }
    

//Setters: ya que los datos son privados, son necesarios para modificar los datos
   /***
    * 
    * @param titulo 
    */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /***
     * 
     * @param prestados 
     */
    public void setPrestados(int prestados) {
        this.prestados = prestados;
    }
  
    public void setAutoria(Autoria autoria) {
        this.autoria = autoria;
    }



//Constructor
    /***
     * 
     * @param titulo
     * @param ejemplares
     * @param prestados 
     */
    public Revista(String titulo, int ejemplares, int prestados) {
        this.titulo = titulo;
        this.prestados = prestados;
    }

    public Revista(String titulo, int prestados) {
        this.titulo = titulo;
        this.prestados = prestados;
    }

    public Revista(String titulo, Autoria autoria) {
        this.titulo = titulo;
        this.autoria = autoria;
    }
    

//Metodos
    /***
     *
     * @return las revistas prestadas
     */
    public int prestar() {
        return prestados++;
    }

    /***
     * 
     * @return las revistas aun prestadas
     */
    public int devolver() {
        if (prestados > 0) {
            prestados--;
            return prestados;
        } else {
            return -1;
        }
    }
}
