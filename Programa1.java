/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package programa1;

import java.util.Scanner;

/**
 *
 * @author EQUIPO
 */
public class Programa1 {

    private static Scanner sc = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        double resultado = 0;//inicializo la variable para poder utilizarla en el primer ejercicio
        double resultado2 = 0;// inicializo la variable para poder utilizarla en el segundo ejecicio
        boolean continuar = true;//condición que va usar el bucle del menú

        while (continuar) {// para que el menú salga infinitas veces 

            System.out.println("1. Calculadora\n"
                    + "2. Ver último resultado de la calculadora\n"
                    + "3. Número primo\n"
                    + "4. Cuadrícula\n"
                    + "5. Pirámide de números\n"
                    + "6. Información de la aplicación\n"
                    + "0. Salir");

            int eleccion = sc.nextInt(); //el número que introduce el usuario para seleccionar una opción del menú

            /*control de errores, para que cuando el usuario introduzca un número negativo
             o mayor de 6, imprima que no exite y le de opción de volver a elegir */
            if (eleccion < 0 || eleccion > 6) {
                System.out.println("No existe esa opción");
            }

            /*Calculadora:En primer lugar, preguntará el primer operando, 
            después el segundo operando, y después la operación, para la que meterá 
            una de las siguientes operaciones posibles: +, -, *, /.*/
            switch (eleccion) {
                case 0:/*Al introducir la opción 0, se terminará el programa.*/
                    continuar = false; //El boolean al ser falso, el bucle cesa.
                    break;

                case 1:
                    System.out.println("Escribe el primer operando");
                    double operando1 = sc.nextDouble();
                    System.out.println("Escribe el segundo operando");
                    double operando2 = sc.nextDouble();
                    System.out.println("Escribe la operación [+,-,*,/]");
                    String operacion = sc.next();

                    /*El usuario introduce todas las variables mediante un scanner,
                excepto "resultado" que ya la he inicializado antes*/
                    double resultadoCalc = calculadora(operando1, operando2, operacion);
                    resultado2 = resultadoCalc;

                    /*inicializo una variable que se usará para llamar a la funcion "calculadora" y para igualar la variable
                que se necesitará para el siguiente ejercicio*/
                    break;
                /*Ver ultimo resultado calculadora: Al introducir la opción 2 del menú, 
                se verá el último resultado de la última operación realizada por la calculadora:*/
                case 2:
                    System.out.println("El último resultado de la calculadora era " + resultado2);

                    /*Se utiliza la variable que se igualó a la de la calculadora en el "if" anterior */
                    break;

                /*Numero primo:
            1) Si elige la primera opción, el programa le pedirá que introduzca un número y 
            le dirá “El número XXXX es primo” o “El número XXX no es primo”, siendo XXXX 
            el número introducido.
            2)Si elige la segunda opción, pedirá que la persona usuaria introduzca un 
            número y mostrará todos los números primos que hay entre el 1 y dicho número introducido.*/
                case 3:
                    System.out.println("¿Que desea saber?\n"
                            + "1. Ver si el número introducido es primo\n"
                            + "2. Mostrar los números primos hasta un valor");
                    /*Muestra el menu específico para la opción 3*/

                    int eleccion2 = sc.nextInt();
                    /*Otra variable para la nueva elección del usuario*/

                    if (eleccion2 == 1) {
                        System.out.println("Introduzca un número");
                        int numero = sc.nextInt();

                        if (primo(numero)) { //llamo a la funcion
                            System.out.println("El número " + numero + " es primo");
                        } else {
                            System.out.println("El número " + numero + " no es primo");

                        }
                    }

                    if (eleccion2 == 2) {
                        primoHasta();//llamo a la funcion
                    }
                    break;

                /*Cuadrícula:se pedirá a la persona usuaria que introduzca el número de filas, 
            después el número de columnas, después el carácter con el que quieres pintar la matriz. 
            El programa pintará una matriz de filas x columnas con el carácter elegido. */
                case 4:
                    System.out.println("Introduzca un número de filas");
                    int filas = sc.nextInt();
                    System.out.println("Introduzca un número de columnas");
                    int columnas = sc.nextInt();
                    System.out.println("Introduzca el caracter con el que quiere pintar la matriz");
                    String caracter = sc.next();

                    cuadricula(filas, columnas, caracter);// Llamo a la funcion
                    break;

                /*Pirámide:introducirá un número e imprimirá una pirámide de números*/
                case 5:
                    System.out.println("Introduzca el tamaño de la piramide de números");
                    int numero = sc.nextInt();

                    piramide(numero);//Llamo a la función  
                    break;
                /*Información de la aplicación*/
                case 6:
                    System.out.println("Datos sobre la programadora:\n"
                            + "Nombre: Cinthya Pérez Vázquez\n"
                            + "Edad: 18 años\n"
                            + "Curso: Primero\n"
                            + "Módulo: DAM Dual"
                    );
                    break;
            }
        }
    }

    /*Ejercicio 1*/
    private static double calculadora(double operando1, double operando2, String operacion) {
        /*declaro una función double(porque los numeros no son enteros) y sus variables*/
        double resultado = 01;
        if (operacion.equals("+")) {// "operacion.equals("+")" determina si el argumento es verdadero (si operacion == +)
            resultado = operando1 + operando2;
            System.out.println("El resultado de " + operando1 + " " + operacion + " " + operando2 + " = " + resultado);
        }

        if (operacion.equals("-")) {
            resultado = operando1 - operando2;
            System.out.println("El resultado de " + operando1 + " " + operacion + " " + operando2 + " = " + resultado);
        }

        if (operacion.equals("*")) {
            resultado = operando1 * operando2;
            System.out.println("El resultado de " + operando1 + " " + operacion + " " + operando2 + " = " + resultado);
        }

        if (operacion.equals("/")) {
            resultado = operando1 / operando2;
            System.out.println("El resultado de " + operando1 + " " + operacion + " " + operando2 + " = " + resultado);
        }
        return resultado;// devuelve el valor de resultado a la función main

    }

    /*Ejercicio 3 (1)*/
    private static boolean primo(int numero) {
        /*declaro una función boolean (valores verdaderos o falsos)*/

        //No he puesto un scanner ya que no pido variables, las recogo de la main 
        if (numero == 0 || numero == 1 || numero == 4) {
            return false;// ninguno de estos numeros son primos 
        }
        for (int x = 2; x < numero / 2; x++) {
            /* Si es divisible por cualquiera de estos números, no
             es primo*/

            if (numero % x == 0) {

                return false;

            }
            // Si no se pudo dividir por ninguno de los de arriba, sí es primo

        }

        return true;//devuelve el valor true para que la función main sepa que es primo
    }

    /*Ejercicio 3 (2)*/
    private static void primoHasta() {
        /*declaro una función void( no necesito que devuelva nada)*/

        System.out.println("Introduzca un número");
        int numero = sc.nextInt();

        for (int j = 2; j < numero; j++) {
            if (primo(j)) {//llama a la función anterior para saber si es primo
                System.out.println(j);//imprime el número primo

            }
        }
    }

    /*Ejercicio 4*/
    private static String cuadricula(int filas, int columnas, String caracter) {
        /*declaro una función String(ya que voy a ejecutarla con la veriable "caracter")*/

        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                System.out.print(caracter); //para que imprima lo que el usuario ha introducido (en la misma línea)
            }
            System.out.println("");
        }
        return caracter;
    }

    /*Ejercicio 5*/
    private static int piramide(int numero) {
        /*declaro una función int(trabaja con números enteros)*/

        if (numero < 0) {//si el número es negativo
            numero = numero * (-1);// Para que se imprima positivo
            for (int i = numero; i >= 1; i--) { // para que empieze desde el número introducido y vaya restando 1 hasta que llegue a 1
                for (int j = i; j >= 1; j--) {// para que vaya decreciendo a medida que la piramide desciende
                    System.out.print(i + " ");
                }
                System.out.println("");//último caracter de las filas
            }
        } else {// si el número es positivo
            for (int i = 1; i <= numero; i++) {// para que empieze desde el 1 y vaya sumando 1 hasta que llegue al número introducido
                for (int j = 1; j <= i; j++) {// para que vaya creciendo a medida que la piramide desciende
                    System.out.print(i + " ");
                }
                System.out.println("");
            }

        }
        return numero;
    }
}
